/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: daleksan <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/04 14:06:59 by daleksan          #+#    #+#             */
/*   Updated: 2017/04/04 14:10:39 by daleksan         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int			count_num(intmax_t number)
{
	unsigned int	i;
	uintmax_t		a;

	i = 0;
	if (number < 0)
	{
		a = -number;
		i++;
	}
	else
		a = number;
	while (a >= 10)
	{
		i++;
		a = a / 10;
	}
	if (a < 10)
		i++;
	return (i);
}

char				*ft_itoa(intmax_t number)
{
	intmax_t		i;
	char			*str;
	uintmax_t		a;
	int				negat;

	negat = 1;
	i = count_num(number);
	if (!(str = (char *)malloc(sizeof(char) * i + 1)))
		return (NULL);
	str[i] = '\0';
	if (number < 0)
	{
		a = -number;
		negat = -1;
	}
	else
		a = number;
	while (i--)
	{
		str[i] = a % 10 + 48;
		a = a / 10;
	}
	if (negat == -1)
		str[0] = '-';
	return (str);
}

void				ia_2(uintmax_t value, unsigned int base, char *str, int *i)
{
	char			*up;

	up = "0123456789ABCDEF";
	if (value >= base)
		ia_2(value / base, base, str, i);
	str[(*i)++] = up[(value % base)];
}

void				ia_b(uintmax_t value, unsigned int base, char *str, int *i)
{
	char			*low;

	low = "0123456789abcdef";
	if (value >= base)
		ia_b(value / base, base, str, i);
	str[(*i)++] = low[(value % base)];
}

char				*ft_itoa_base(uintmax_t value, unsigned int base, int m)
{
	int				i;
	char			*str;

	i = 0;
	if (base < 2 || base > 16 || !(str = (char*)malloc(32)))
		return (0);
	m > 0 ? ia_2(value, base, str, &i) : ia_b(value, base, str, &i);
	str[i] = '\0';
	return (str);
}
